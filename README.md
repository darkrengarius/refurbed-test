# Notifier

This package provides notifier to use as a library along with the simple CLI using this notifier. 

Notifier is intended to sent out notifications. The package ships notifier interface and its HTTP implementation.

## HTTP Notifier

HTTP implementation of notifier sends notification to the specified URL via POST method with `Content-Type: text/plain` header attached. The `Notify` operation is not blocking and is intended to be so, so all 3rd party implementations should take this into account.

## CLI

CLI documentation is located under `cmd/notify`.

## Prerequisites

Required:

- Go 1.13+.

Not required:

- `vektra/mockery` to use `generate` make target. This is not needed, cause all mocks are already generated, but if one wants to execute this target, mockery should be installed. Installation guide can be found here: https://github.com/vektra/mockery;
- `golangci-lint` to use `lint` make target. Installation guide can be found here: https://github.com/golangci/golangci-lint.

## How To Launch

Simply run `make build`. This will get you a binary under `./bin/notify` directory. 