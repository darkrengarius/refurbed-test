# Notifier CLI

Notifier or `notify` is used to send out notifications taken from OS standard input. CLI uses HTTP implementation of notifier.

## Flags

- `url` (short `u`) - sets URL to send notifications to. Required;
- `interval` (short `i`) sets interval between requests. Interval is passed in Go `time.Duration` string format.

## Example Usage

```bash
$ ./notify -u https://example.com -i 10s < ./notifications_file
```