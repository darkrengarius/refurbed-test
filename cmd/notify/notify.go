package main

import "gitlab.com/darkrengarius/refurbed-test/cmd/notify/commands"

func main() {
	commands.Execute()
}
