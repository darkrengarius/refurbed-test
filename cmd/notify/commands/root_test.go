package commands

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/darkrengarius/refurbed-test/pkg/async"
	"gitlab.com/darkrengarius/refurbed-test/pkg/notifier"
)

func TestMain(m *testing.M) {
	// suppress logging output for tests
	logger.SetOutput(ioutil.Discard)

	os.Exit(m.Run())
}

type infiniteReader struct {
	data          uint64
	generatedData map[string]int
}

func newInfiniteReader() *infiniteReader {
	return &infiniteReader{
		generatedData: make(map[string]int),
	}
}

func (r *infiniteReader) Read(p []byte) (int, error) {
	next := strconv.FormatUint(r.data, 10)
	r.data++

	// next may not be copied entirely, but it doesn't matter,
	// we just ensure some variety on the output, that's enough
	n := copy(p, next+"\n")

	r.generatedData[next[:n-1]]++

	return n, nil
}

func TestSendOutNotifications(t *testing.T) {
	var (
		receivedNotificationsMx sync.Mutex
		receivedNotifications   []string
	)

	n := &notifier.MockNotifier{}
	n.On("Notify", mock.AnythingOfType("string")).Return(func(notification string) *async.Error {
		receivedNotificationsMx.Lock()
		receivedNotifications = append(receivedNotifications, notification)
		receivedNotificationsMx.Unlock()

		asyncErr := async.NewError()

		parsedNotification, err := strconv.Atoi(notification)
		if err != nil {
			asyncErr.SetError(fmt.Errorf("failed to parse notification: %w", err))
			return asyncErr
		}

		if parsedNotification%2 == 0 {
			asyncErr.SetError(nil)
			return asyncErr
		}

		asyncErr.SetError(assert.AnError)
		return asyncErr
	})

	const interval = 100 * time.Millisecond

	t.Run("no interruption", func(t *testing.T) {
		const nCount = 10

		wantNotifications := make(map[string]struct{}, nCount)
		buf := bytes.NewBuffer(nil)
		for i := 0; i < nCount; i++ {
			notification := strconv.Itoa(i)

			_, err := buf.WriteString(notification + "\n")
			require.NoError(t, err)

			wantNotifications[notification] = struct{}{}
		}

		sendOutNotifications(context.Background(), n, buf, interval)

		receivedNotificationsMx.Lock()
		for _, n := range receivedNotifications {
			delete(wantNotifications, n)
		}
		receivedNotifications = receivedNotifications[:0]
		receivedNotificationsMx.Unlock()

		require.Len(t, wantNotifications, 0)
	})

	t.Run("interrupted", func(t *testing.T) {
		r := newInfiniteReader()

		ctx, cancel := context.WithCancel(context.Background())

		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()

			sendOutNotifications(ctx, n, r, interval)
		}()

		// let it run for a while
		time.Sleep(1 * time.Second)

		cancel()
		wg.Wait()

		receivedNotificationsMx.Lock()
		for _, n := range receivedNotifications {
			r.generatedData[n]--
			if r.generatedData[n] == 0 {
				delete(r.generatedData, n)
			}
		}
		receivedNotifications = receivedNotifications[:0]
		receivedNotificationsMx.Unlock()

		require.Len(t, r.generatedData, 0)
	})
}
