package commands

import (
	"gitlab.com/darkrengarius/refurbed-test/pkg/async"
	"gitlab.com/darkrengarius/refurbed-test/pkg/concurrent"
)

type notificationError struct {
	notification string
	asyncErr     *async.Error
}

type notificationErrorQueue struct {
	q *concurrent.Queue
}

func newNotificationErrorQueue() *notificationErrorQueue {
	return &notificationErrorQueue{
		q: concurrent.NewQueue(),
	}
}

func (q *notificationErrorQueue) push(e *notificationError) {
	q.q.Push(e)
}

func (q *notificationErrorQueue) waitPop() *notificationError {
	return q.assertValueType(q.q.WaitPop())
}

func (q *notificationErrorQueue) close() {
	q.q.Close()
}

func (q *notificationErrorQueue) assertValueType(eIfc interface{}) *notificationError {
	if eIfc == nil {
		return nil
	}

	e, ok := eIfc.(*notificationError)
	if !ok {
		// shouldn't happen if we use queue's API,
		// but just in case
		return nil
	}

	return e
}
