package commands

import (
	"bufio"
	"context"
	"io"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/darkrengarius/refurbed-test/pkg/notifier"
	"gitlab.com/darkrengarius/refurbed-test/pkg/notifier/httpnotifier"
)

const (
	defaultInterval = "5s"
)

var (
	url         string
	intervalStr string
)

var (
	logger = logrus.New()
)

var rootCmd = &cobra.Command{
	Use:   "notify",
	Short: "Sends out notifications",
	Run: func(_ *cobra.Command, args []string) {
		interval, err := time.ParseDuration(intervalStr)
		if err != nil {
			logger.WithError(err).Fatalln("Invalid interval")
		}

		ctx, cancel := context.WithCancel(context.Background())

		osSigs := make(chan os.Signal)
		signal.Notify(osSigs, syscall.SIGINT)

		go func() {
			<-osSigs
			cancel()
		}()

		n := httpnotifier.New(logger, url)

		sendOutNotifications(ctx, n, os.Stdin, interval)
	},
}

// Execute runs root CLI command.
func Execute() {
	rootCmd.Flags().StringVarP(&url, "url", "u", "", "URL to send notifications to")
	rootCmd.Flags().StringVarP(&intervalStr, "interval", "i", defaultInterval, "notification interval")
	if err := rootCmd.MarkFlagRequired("url"); err != nil {
		logger.WithError(err).Fatalln("Failed to mark url flag required")
	}

	if err := rootCmd.Execute(); err != nil {
		logger.WithError(err).Fatalln("Failed to execute command")
	}
}

func sendOutNotifications(ctx context.Context, n notifier.Notifier, src io.Reader, interval time.Duration) {
	s := bufio.NewScanner(src)

	nErrQueue := newNotificationErrorQueue()

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()

		for nErr := nErrQueue.waitPop(); nErr != nil; nErr = nErrQueue.waitPop() {
			if err := nErr.asyncErr.Await(); err != nil {
				logger.WithError(err).WithField("notification", nErr.notification).
					Errorln("Failed to send notification")
			}
		}
	}()

	sendTicker := time.NewTicker(interval)
	defer sendTicker.Stop()

	for s.Scan() {
		notification := s.Text()

		asyncErr := n.Notify(notification)
		nErrQueue.push(&notificationError{
			notification: notification,
			asyncErr:     asyncErr,
		})

		select {
		case <-ctx.Done():
			nErrQueue.close()
			// wait till error queue is drained
			wg.Wait()
			return
		case <-sendTicker.C:
		}
	}
}
