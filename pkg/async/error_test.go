package async

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewError(t *testing.T) {
	err := NewError()
	require.NotNil(t, err)
	require.NotNil(t, err.errC)
}

func TestError_Await(t *testing.T) {
	t.Run("serial run", func(t *testing.T) {
		asyncErr := NewError()

		var isSetMx sync.Mutex
		isSet := false
		go func() {
			time.Sleep(3 * time.Second)

			isSetMx.Lock()
			asyncErr.SetError(assert.AnError)
			isSet = true
			isSetMx.Unlock()
		}()

		err := asyncErr.Await()
		// ensure that error was actually set by goroutine
		isSetMx.Lock()
		require.True(t, isSet)
		isSetMx.Unlock()
		require.Equal(t, assert.AnError, err)

		// we acquire mutex to ensure that `Await` call doesn't block on it
		// if error is already set
		asyncErr.errMx.Lock()
		err = asyncErr.Await()
		asyncErr.errMx.Unlock()
		require.Equal(t, assert.AnError, err)
	})

	t.Run("concurrent run", func(t *testing.T) {
		asyncErr := NewError()

		const goroutinesCount = 100

		gotErrs := make(chan error)

		for i := 0; i < goroutinesCount; i++ {
			go func() {
				err := asyncErr.Await()
				gotErrs <- err
			}()
		}

		go func() {
			time.Sleep(3 * time.Second)

			asyncErr.SetError(assert.AnError)
		}()

		for i := 0; i < goroutinesCount; i++ {
			gotErr := <-gotErrs
			require.Equal(t, assert.AnError, gotErr)
		}

		close(gotErrs)
	})
}

func TestError_SetError(t *testing.T) {
	asyncErr := NewError()

	asyncErr.SetError(assert.AnError)
	err := asyncErr.Await()
	require.Equal(t, assert.AnError, err)
	require.Equal(t, assert.AnError, asyncErr.err)

	// shouldn't fail and shouldn't change value
	asyncErr.SetError(fmt.Errorf("%w modified", assert.AnError))
	require.Equal(t, assert.AnError, asyncErr.err)
}

func TestError_Error(t *testing.T) {
	t.Run("non-nil error", func(t *testing.T) {
		asyncErr := NewError()

		asyncErr.SetError(assert.AnError)

		errStr := asyncErr.Error()
		require.Equal(t, assert.AnError.Error(), errStr)
	})

	t.Run("nil error", func(t *testing.T) {
		asyncErr := NewError()

		asyncErr.SetError(nil)

		errStr := asyncErr.Error()
		require.Equal(t, nilErrString, errStr)
	})
}
