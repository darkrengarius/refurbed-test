package async

import (
	"sync"
	"sync/atomic"
)

const (
	nilErrString = "<nil>"
)

// Error is an asynchronous error.
type Error struct {
	errMx      sync.Mutex
	err        error
	isResolved int32
	setErrOnce sync.Once
	errC       chan error
}

// NewError constructs new async error.
func NewError() *Error {
	return &Error{
		// we set 1-len buffer to ensure that error-setting code
		// won't block if `Await` is never called
		errC: make(chan error, 1),
	}
}

// Await blocks until the actual error value is set. After value is set,
// all sequential calls will return without blocking.
func (e *Error) Await() error {
	// in general, sequential calls to `Await` will terminate here,
	// only checking the `isResolved` value, without acquiring mutex,
	// saving us some precious ns
	if e.getIsResolved() {
		return e.err
	}

	// all calls that get here before error gets resolved
	// will naturally lock
	e.errMx.Lock()
	defer e.errMx.Unlock()

	// only the first one here will get the actual error,
	// others will get `ok = false` and will just return the
	// already resolved error
	err, ok := <-e.errC
	if !ok {
		return e.err
	}

	e.err = err
	e.setIsResolved()

	return err
}

// SetError sets error value. Does not block even if `Await` is not called.
func (e *Error) SetError(err error) {
	// we ensure that this code is only called once
	e.setErrOnce.Do(func() {
		e.errC <- err
		close(e.errC)
	})
}

// Error implements `error` for the internal error value. Blocks if
// value is not yet set.
func (e *Error) Error() string {
	if err := e.Await(); err != nil {
		return err.Error()
	}

	return nilErrString
}

func (e *Error) getIsResolved() bool {
	return atomic.LoadInt32(&e.isResolved) == 1
}

func (e *Error) setIsResolved() {
	atomic.StoreInt32(&e.isResolved, 1)
}
