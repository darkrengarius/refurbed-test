// Package httpnotifier provides `notifier.Notifier` implementation
// which utilizes HTTP protocol for transferring notifications.
package httpnotifier

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"

	"gitlab.com/darkrengarius/refurbed-test/pkg/async"
)

var (
	httpCl = &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 1024,
		},
	}
)

// HTTPNotifier implements `notifier.Notifier` utilizing HTTP
// for transferring notifications. Count of simultaneous requests
// is limited by the argument passed to constructor. Otherwise it falls
// back to default value.
type HTTPNotifier struct {
	log       *logrus.Logger
	url       string
	queue     *notificationJobQueue
	sendersWg sync.WaitGroup
}

// New constructs new HTTP notifier.
func New(l *logrus.Logger, url string, opts ...Option) *HTTPNotifier {
	options := defaultOptions()

	for _, o := range opts {
		o.apply(&options)
	}

	n := &HTTPNotifier{
		log:   l,
		url:   url,
		queue: newNotificationJobQueue(),
	}

	// we control outbound traffic by limiting the count of sending goroutines
	n.sendersWg.Add(options.simultaneousRequestsLimit)
	for i := 0; i < options.simultaneousRequestsLimit; i++ {
		go n.sendLoop()
	}

	return n
}

// Notify implements `notifier.Notifier`. Requests are performed
// asynchronously, so call of this function does not block.
func (n *HTTPNotifier) Notify(notification string) *async.Error {
	err := async.NewError()

	job := &notificationJob{
		notification: notification,
		err:          err,
	}

	n.queue.push(job)

	return err
}

// Close closes notifier and cleans up resources.
func (n *HTTPNotifier) Close() {
	n.queue.close()
	n.sendersWg.Wait()
}

func (n *HTTPNotifier) sendLoop() {
	defer n.sendersWg.Done()

	for j := n.queue.waitPop(); j != nil; j = n.queue.waitPop() {
		n.send(j)
	}
}

func (n *HTTPNotifier) send(j *notificationJob) {
	req, err := http.NewRequest(http.MethodPost, n.url, strings.NewReader(j.notification))
	if err != nil {
		j.err.SetError(fmt.Errorf("failed to construct request: %w", err))
		return
	}

	req.Header.Set("Content-Type", "plain/text")

	resp, err := httpCl.Do(req)
	if err != nil {
		j.err.SetError(fmt.Errorf("failed to perform request: %w", err))
		return
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			n.log.WithError(err).Errorln("Failed to close response body")
		}
	}()

	// even if we don't need the response body, we should read in order to abuse keep-alive.
	// For reference, look at the comment on `Body` field of the response struct:
	// https://golang.org/pkg/net/http/#Response
	if _, err := io.Copy(ioutil.Discard, resp.Body); err != nil {
		j.err.SetError(fmt.Errorf("failed to read response body: %w", err))
	}

	if resp.StatusCode != http.StatusOK {
		j.err.SetError(fmt.Errorf("got status %d in response", resp.StatusCode))
		return
	}

	j.err.SetError(nil)
}
