package httpnotifier

const (
	// DefaultSimultaneousRequestsLimit is a default simultaneous requests limit.
	DefaultSimultaneousRequestsLimit = 1000
)

type options struct {
	simultaneousRequestsLimit int
}

func defaultOptions() options {
	return options{
		simultaneousRequestsLimit: DefaultSimultaneousRequestsLimit,
	}
}

// Option is an optional parameter for HTTP notifier constructor.
type Option interface {
	apply(o *options)
}

type optionFunc func(o *options)

func (f optionFunc) apply(o *options) {
	f(o)
}

// WithSimultaneousRequestsLimit provides simultaneous requests limit to
// the notifier constructor.
func WithSimultaneousRequestsLimit(limit int) Option {
	return optionFunc(func(o *options) {
		o.simultaneousRequestsLimit = limit
	})
}
