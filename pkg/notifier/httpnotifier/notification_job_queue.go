package httpnotifier

import (
	"gitlab.com/darkrengarius/refurbed-test/pkg/async"
	"gitlab.com/darkrengarius/refurbed-test/pkg/concurrent"
)

type notificationJob struct {
	notification string
	err          *async.Error
}

// notificationJobQueue simply wraps `concurrent.Queue` allowing
// encapsulating type assertion for easier use.
type notificationJobQueue struct {
	q *concurrent.Queue
}

func newNotificationJobQueue() *notificationJobQueue {
	return &notificationJobQueue{
		q: concurrent.NewQueue(),
	}
}

func (q *notificationJobQueue) push(j *notificationJob) {
	q.q.Push(j)
}

func (q *notificationJobQueue) waitPop() *notificationJob {
	return q.assertValueType(q.q.WaitPop())
}

func (q *notificationJobQueue) close() {
	q.q.Close()
}

func (q *notificationJobQueue) assertValueType(jIfc interface{}) *notificationJob {
	if jIfc == nil {
		return nil
	}

	j, ok := jIfc.(*notificationJob)
	if !ok {
		// shouldn't happen if we use queue's API,
		// but just in case
		return nil
	}

	return j
}
