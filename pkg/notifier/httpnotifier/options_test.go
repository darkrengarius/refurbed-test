package httpnotifier

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestWithSendersCount(t *testing.T) {
	options := defaultOptions()

	opt := WithSimultaneousRequestsLimit(100)
	opt.apply(&options)

	require.Equal(t, 100, options.simultaneousRequestsLimit)
}
