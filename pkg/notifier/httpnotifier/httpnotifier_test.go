package httpnotifier

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

const url = "https://example.com"

func TestNew(t *testing.T) {
	n := New(logrus.New(), url)
	require.NotNil(t, n)
	require.Equal(t, n.url, url)
	require.NotNil(t, n.queue)

	n.Close()
}

func TestHTTPNotifier_Close(t *testing.T) {
	n := New(logrus.New(), url)

	n.Close()
	// sequential calls shouldn't fail
	n.Close()
}

func TestHTTPNotifier_Notify(t *testing.T) {
	anError := fmt.Errorf("got status %d in response", http.StatusAlreadyReported)

	t.Run("serial run", func(t *testing.T) {
		server := prepareTestServer(1 * time.Second)
		defer server.Close()

		n := New(logrus.New(), server.URL, WithSimultaneousRequestsLimit(5))
		defer n.Close()

		err := n.Notify("0")
		require.NoError(t, err.Await())

		err = n.Notify("1")
		// this one should wait a bit
		require.Equal(t, err.Error(), anError.Error())

		err = n.Notify("2")
		require.Equal(t, err.Error(), anError.Error())
	})

	t.Run("concurrent run", func(t *testing.T) {
		server := prepareTestServer(100 * time.Millisecond)
		defer server.Close()

		n := New(logrus.New(), server.URL)
		defer n.Close()

		const (
			goroutinesCount          = 100
			notificationPerGoroutine = 100
		)

		type testCase struct {
			gotErr  error
			wantErr error
		}

		testCaseC := make(chan testCase)
		for i := 0; i < goroutinesCount; i++ {
			go func() {
				rnd := rand.New(rand.NewSource(time.Now().UnixNano())) //nolint:gosec

				for i := 0; i < notificationPerGoroutine; i++ {
					notification := strconv.Itoa(rnd.Intn(3))

					var wantErr error
					if notification != "0" {
						wantErr = anError
					}

					err := n.Notify(notification)
					testCaseC <- testCase{
						gotErr:  err.Await(),
						wantErr: wantErr,
					}
				}
			}()
		}

		for i := 0; i < goroutinesCount*notificationPerGoroutine; i++ {
			testCase := <-testCaseC
			require.Equal(t, testCase.wantErr, testCase.gotErr)
		}
		close(testCaseC)
	})
}

func prepareTestServer(awaitTimeout time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		notification := string(body)

		switch notification {
		case "0":
			w.WriteHeader(http.StatusOK)
		case "1":
			// test error's `Await`
			time.Sleep(awaitTimeout)
			w.WriteHeader(http.StatusAlreadyReported)
		default:
			w.WriteHeader(http.StatusAlreadyReported)
		}
	}))
}
