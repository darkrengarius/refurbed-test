// Package notifier provides a way to notify outer entities.
package notifier

import "gitlab.com/darkrengarius/refurbed-test/pkg/async"

//go:generate mockery -name Notifier -case underscore -inpkg

// Notifier sends out notifications.
type Notifier interface {
	// Notify sends `notification` to the outer entity. It returns
	// asynchronous error, so implementations should make this call
	// not blocking.
	Notify(notification string) *async.Error
}
