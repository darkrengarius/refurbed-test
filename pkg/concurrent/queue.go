package concurrent

import "sync"

// Queue is a concurrent-safe queue of arbitrary data-type values.
type Queue struct {
	dataGuard *sync.Cond
	data      []interface{}
	closeOnce sync.Once
	closeC    chan struct{}
}

// NewQueue constructs new queue.
func NewQueue() *Queue {
	return &Queue{
		dataGuard: sync.NewCond(&sync.Mutex{}),
		closeC:    make(chan struct{}),
	}
}

// Push pushes new value to the queue.
func (q *Queue) Push(v interface{}) {
	if q.isClosed() {
		// no need to store new values in this case
		return
	}

	q.dataGuard.L.Lock()
	q.data = append(q.data, v)
	q.dataGuard.Broadcast()
	q.dataGuard.L.Unlock()
}

// Pop pops value from the queue. Returns nil if queue is empty.
func (q *Queue) Pop() interface{} {
	q.dataGuard.L.Lock()
	defer q.dataGuard.L.Unlock()

	return q.pop()
}

// WaitPop blocks until there is a value in the queue. One should be
// aware that goroutine calling this function will block entirely either
// until value arrives or `Close` method is called. In case `Close` is called
// if there is a lot of data in the queue, all reading goroutines are allowed
// to drain the queue. Function returns nil when queue is empty.
func (q *Queue) WaitPop() interface{} {
	if q.isClosed() {
		// queue is already closed, but we let caller drain it
		return q.Pop()
	}

	q.dataGuard.L.Lock()
	defer q.dataGuard.L.Unlock()

	// if queue gets closed during waiting, we'll still
	// call `pop` to let caller drain it
	for len(q.data) == 0 && !q.isClosed() {
		q.dataGuard.Wait()
	}

	return q.pop()
}

// Close unblocks all the goroutines blocked on `WaitPop` call. Queue in the
// closed stated does not accept new values.
func (q *Queue) Close() {
	q.closeOnce.Do(func() {
		q.dataGuard.L.Lock()
		close(q.closeC)
		q.dataGuard.Broadcast()
		q.dataGuard.L.Unlock()
	})
}

func (q *Queue) pop() interface{} {
	if len(q.data) == 0 {
		return nil
	}

	v := q.data[0]
	q.data = q.data[1:]

	return v
}

func (q *Queue) isClosed() bool {
	select {
	case <-q.closeC:
		return true
	default:
	}

	return false
}
