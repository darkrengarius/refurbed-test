package concurrent

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestNewQueue(t *testing.T) {
	q := NewQueue()
	require.NotNil(t, q)
	require.Len(t, q.data, 0)
	require.NotNil(t, q.dataGuard)
	require.NotNil(t, q.closeC)

	q.Close()
}

func TestQueue_Push(t *testing.T) {
	t.Run("queue not closed, serial run", func(t *testing.T) {
		q := NewQueue()
		defer q.Close()

		data := []int{1, 2, 3, 4, 5}

		for _, v := range data {
			q.Push(v)
		}

		require.Len(t, q.data, len(data))

		for i := range q.data {
			require.Equal(t, data[i], q.data[i])
		}
	})

	t.Run("queue not closed, concurrent run", func(t *testing.T) {
		q := NewQueue()
		defer q.Close()

		const dataCount = 1000

		data := make([]int, 0, dataCount)
		for i := 0; i < dataCount; i++ {
			data = append(data, i)
		}

		const goroutinesCount = 100

		wantData := make(map[int]int, dataCount)
		for i := 0; i < dataCount; i++ {
			wantData[data[i]] = goroutinesCount
		}

		var wg sync.WaitGroup
		wg.Add(goroutinesCount)
		for i := 0; i < goroutinesCount; i++ {
			go func() {
				defer wg.Done()

				for i := 0; i < dataCount; i++ {
					q.Push(data[i])
				}
			}()
		}
		wg.Wait()

		require.Len(t, q.data, goroutinesCount*dataCount)

		for i := 0; i < goroutinesCount*dataCount; i++ {
			val, ok := q.data[i].(int)
			require.True(t, ok)

			wantData[val]--
			if wantData[val] == 0 {
				delete(wantData, val)
			}
		}

		require.Len(t, wantData, 0)
	})

	t.Run("closed queue", func(t *testing.T) {
		q := NewQueue()
		q.Push(1)
		q.Close()

		q.Push(2)
		require.Len(t, q.data, 1)
		require.Equal(t, q.data[0], 1)
	})
}

func TestQueue_Pop(t *testing.T) {
	t.Run("serial run", func(t *testing.T) {
		q := NewQueue()

		data := []int{1, 2, 3, 4, 5}
		for _, v := range data {
			q.Push(v)
		}

		i := 0
		for valIfc := q.Pop(); valIfc != nil; valIfc = q.Pop() {
			val, ok := valIfc.(int)
			require.True(t, ok)

			require.Equal(t, data[i], val)

			i++
		}

		require.Len(t, q.data, 0)

		q.Push(1)
		q.Push(2)

		q.Close()

		// test queue draining
		require.Equal(t, 1, q.Pop())
		require.Equal(t, 2, q.Pop())
		require.Nil(t, q.Pop())
	})

	t.Run("concurrent run", func(t *testing.T) {
		q := NewQueue()
		defer q.Close()

		const dataCount = 1000

		data := make([]int, 0, dataCount)
		for i := 0; i < dataCount; i++ {
			data = append(data, i)
		}

		const goroutinesCount = 100

		wantData := make(map[int]int, dataCount)
		for i := 0; i < dataCount; i++ {
			for j := 0; j < goroutinesCount; j++ {
				q.Push(i)
			}

			wantData[data[i]] = goroutinesCount
		}

		valC := make(chan int)
		for i := 0; i < goroutinesCount; i++ {
			go func() {
				for valIfc := q.Pop(); valIfc != nil; valIfc = q.Pop() {
					val, ok := valIfc.(int)
					require.True(t, ok)

					valC <- val
				}
			}()
		}

		for i := 0; i < dataCount*goroutinesCount; i++ {
			val := <-valC

			wantData[val]--
			if wantData[val] == 0 {
				delete(wantData, val)
			}
		}

		require.Len(t, wantData, 0)
	})
}

func TestQueue_WaitPop(t *testing.T) {
	t.Run("serial run", func(t *testing.T) {
		q := NewQueue()

		data := []int{1, 2, 3, 4, 5}
		for _, v := range data {
			q.Push(v)
		}

		for i := 0; i < len(data); i++ {
			valIfc := q.WaitPop()

			val, ok := valIfc.(int)
			require.True(t, ok)

			require.Equal(t, data[i], val)
		}

		require.Len(t, q.data, 0)

		valC := make(chan interface{})
		go func() {
			valC <- q.WaitPop()
		}()

		// let goroutine wait a bit
		time.Sleep(1 * time.Second)

		q.Push(1)
		val := <-valC
		require.Equal(t, val, 1)
		close(valC)

		q.Push(1)
		q.Push(2)

		q.Close()

		// test queue draining
		require.Equal(t, 1, q.WaitPop())
		require.Equal(t, 2, q.WaitPop())
		require.Nil(t, q.WaitPop())
	})

	t.Run("concurrent run", func(t *testing.T) {
		q := NewQueue()

		const dataCount = 1000

		data := make([]int, 0, dataCount)
		for i := 0; i < dataCount; i++ {
			data = append(data, i)
		}

		const goroutinesCount = 100

		wantData := make(map[int]int, dataCount)
		for i := 0; i < dataCount; i++ {
			for j := 0; j < goroutinesCount; j++ {
				q.Push(i)
			}

			wantData[data[i]] = goroutinesCount
		}

		valC := make(chan int)
		var wg sync.WaitGroup
		wg.Add(goroutinesCount)
		for i := 0; i < goroutinesCount; i++ {
			go func() {
				defer wg.Done()

				for valIfc := q.WaitPop(); valIfc != nil; valIfc = q.WaitPop() {
					val, ok := valIfc.(int)
					require.True(t, ok)

					valC <- val
				}
			}()
		}

		for i := 0; i < dataCount*goroutinesCount; i++ {
			val := <-valC

			wantData[val]--
			if wantData[val] == 0 {
				delete(wantData, val)
			}
		}

		require.Len(t, wantData, 0)

		q.Close()
		wg.Wait()
	})
}

func TestQueue_Close(t *testing.T) {
	q := NewQueue()

	q.Push(1)
	q.Push(2)

	q.Close()

	require.Len(t, q.data, 2)
	require.Equal(t, q.data[0], 1)
	require.Equal(t, q.data[1], 2)

	q.Close()
}
