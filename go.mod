module gitlab.com/darkrengarius/refurbed-test

go 1.14

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1
)
