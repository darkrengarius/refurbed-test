dep:
	go mod tidy
	go mod vendor -v

test:
	go test -race ./...

lint:
	golangci-lint run -c .golangci.yml ./...

generate:
	PATH=$$PATH:$$HOME/go/bin go generate ./pkg/...

clean:
	rm -rf ./bin
	mkdir ./bin

build: clean
	go build -mod=vendor -o ./bin/notify/notify ./cmd/notify
